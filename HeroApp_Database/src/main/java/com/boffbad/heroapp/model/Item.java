package com.boffbad.heroapp.model;

import java.util.List;

public class Item {

    private Integer id;
    private String nom;
    private List<Caracteristique> listeCaracteristique;
    private int nbUtilisations;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Caracteristique> getListeCaracteristique() {
        return listeCaracteristique;
    }

    public void setListeCaracteristique(List<Caracteristique> listeCaracteristique) {
        this.listeCaracteristique = listeCaracteristique;
    }

    public int getNb_Utilisations() {
        return nbUtilisations;
    }

    public void setNb_Utilisations(int nbUtilisations) {
        this.nbUtilisations = nbUtilisations;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return nom;
    }

}
