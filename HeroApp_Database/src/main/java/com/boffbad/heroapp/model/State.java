package com.boffbad.heroapp.model;

import java.util.List;

public class State {

    int id;
    private String nom;
    private List<Caracteristique> listeCaracteristique;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Caracteristique> getListeCaracteristique() {
        return listeCaracteristique;
    }

    public void setListeCaracteristique(List<Caracteristique> listeCaracteristique) {
        this.listeCaracteristique = listeCaracteristique;
    }

}
