package com.boffbad.heroapp.model;

import java.util.List;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "paragraphe")
public class Paragraphe {

    @Id
    String id;
    int numero;
    String texte;
    List<Embranchement> listEmbranchements;
    Histoire histoire;
    String chapitre;
    String partie;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public List<Embranchement> getListEmbranchements() {
        return listEmbranchements;
    }

    public void setListEmbranchements(List<Embranchement> listEmbranchements) {
        this.listEmbranchements = listEmbranchements;
    }

    public Histoire getHistoire() {
        return histoire;
    }

    public void setHistoire(Histoire histoire) {
        this.histoire = histoire;
    }

    @Override
    public String toString() {
        return String.valueOf(numero + " " + partie);
    }

    public String getChapitre() {
        return chapitre;
    }

    public void setChapitre(String chapitre) {
        this.chapitre = chapitre;
    }

    public String getPartie() {
        return partie;
    }

    public void setPartie(String partie) {
        this.partie = partie;
    }

}
