package com.boffbad.heroapp.model;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "embranchement")
public class Embranchement {

    @Id
    String id;

    Integer numParagrapheSource;
    Integer numParagrapheSucces;
    // Integer numParagrapheEchec;
    // String idObjetNecessaire;
    // String idCaracteristique;
    // Integer bonusSituationnel;
    String idHistoire;
    // Difficulte difficulte;
    String texteAction;

    public String getId() {
        return id;
    }

    public void setId(String string) {
        this.id = string;
    }

    public Integer getNumParagrapheSucces() {
        return numParagrapheSucces;
    }

    public void setNumParagrapheSucces(Integer numParagrapheSucces) {
        this.numParagrapheSucces = numParagrapheSucces;
    }

    public String getIdHistoire() {
        return idHistoire;
    }

    public void setIdHistoire(String idHistoire) {
        this.idHistoire = idHistoire;
    }

    public Integer getNumParagrapheSource() {
        return numParagrapheSource;
    }

    public void setNumParagrapheSource(Integer numParagrapheSource) {
        this.numParagrapheSource = numParagrapheSource;
    }

    public String getTexteAction() {
        return texteAction;
    }

    public void setTexteAction(String texteAction) {
        this.texteAction = texteAction;
    }

}
