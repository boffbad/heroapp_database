package demo;

import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "message")
public class Message {
    @Id
    private String id;

    private String contenu;

    // @Indexed(unique=false)
    // private User createur;
    //
    // @Indexed(unique=false)
    // private Topic topic;

    private Date dateMessage;

    public Message() {
        super();
    }

    public Message(String contenu) {
        this.contenu = contenu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    // public User getCreateur() {
    // return createur;
    // }
    //
    // public void setCreateur(User createur) {
    // this.createur = createur;
    // }
    //
    // public Topic getTopic() {
    // return topic;
    // }
    //
    // public void setTopic(Topic topic) {
    // this.topic = topic;
    // }

    public Date getDateMessage() {
        return dateMessage;
    }

    public void setDateMessage(Date dateMessage) {
        this.dateMessage = dateMessage;
    }

}