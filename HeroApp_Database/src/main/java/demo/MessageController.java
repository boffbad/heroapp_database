package demo;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class MessageController {

    @Autowired
    MessageRepository messageRepository;

    @GetMapping("/message")
    public List<Message> getAllMessage() {
        // Sort sortByCreatedAtDesc = new Sort(Sort.Direction.DESC, "createdAt");
        return messageRepository.findAll();
    }

    @PostMapping("/message")
    public Message createMessage(@Valid @RequestBody Message message) {
        return messageRepository.save(message);
    }

    @GetMapping(value = "/message/id/{id}")
    public Optional<Message> getMessageById(@PathVariable("id") String id) {
        return messageRepository.findById(id);
    }

    // @GetMapping(value="/message/libelle/{libelle}")
    // public ResponseEntity<List<Message>>
    // getMessageByLibelle(@PathVariable("libelle") String libelle) {
    // List<Message> message = messageRepository.findByLibelle(libelle);
    // if(message != null) {
    // return ResponseEntity.ok().body(message);
    // }else {
    // return ResponseEntity.notFound().build();
    // }
    //
    //// .map(message -> ResponseEntity.ok().body(message))
    //// .orElse(ResponseEntity.notFound().build());
    // }

    // @GetMapping(value="/message/libelle/{libelle}")
    // public Message getMessageByLibelle(@PathVariable("libelle") String libelle) {
    // return messageRepository.findByLibelle(libelle);
    // }

    // @GetMapping("/message/topic/{topic}")
    // public List<Message> getMessagesByTopic(@PathVariable("topic") Topic topic) {
    // return messageRepository.findByTopic(topic);
    // }

    @PutMapping(value = "/message/{id}")
    public ResponseEntity<Message> updateMessage(@PathVariable("id") String id, @Valid @RequestBody Message message) {
        return messageRepository.findById(id).map(messageData -> {
            messageData.setContenu(message.getContenu());
            // messageData.setTitle(message.getTitle());
            // messageData.setCompleted(message.getCompleted());
            Message updatedMessage = messageRepository.save(messageData);
            return ResponseEntity.ok().body(updatedMessage);
        }).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(value = "/message/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable("id") String id) {
        return messageRepository.findById(id).map(message -> {
            messageRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }).orElse(ResponseEntity.notFound().build());
    }
}